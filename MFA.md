### Configuration

- MFA configuration requires setup of configuration for every step in authentication.multiStepAuthCalls in adapter's configuration
- An configuration item of authentication.multiStepAuthCalls contains:
    - **name**, id of the step call
    - **requestFields**, the fields set in step request
    - **responseFields**, the fields from the step result
    - **successfullResponseCode**, expected response code for given step, if not set any successfull http response is accepted
- every MFA step has to have corresponding 2 entity files under /entities/.system:
    - the request file: `schemaTokenReq_MFA_Step_[number]`
    - the response file: `schemaTokenResp_MFA_Step_[number]`

###### name

Step `name` is used by other steps to reference response values

###### requestFields

Request field name if prefixed with `header` .e.g: `header.jx-session` means the request will be sent with http header `jx-session`. Otherwise the field is placed in request body.

Any step can set the value of its request field to:
- plain value e.g. `"password": "Alice-secret"`
- referenced value from other step e.g. `"jx-session": "{getSession.responseFields.session}"`, `"header.Authorization": "Bearer {getAccessToken.responseFields.accessToken}"`, `"header.Cookie": "session={getSession.responseFields.session}"`
The reference consists of three parts separated by dots: `{[step_name].responseFields.[field_name]}`, that allows 
to identify the value to be placed into this request field.
**Important**: if the value of a request field is set to a referenced value from previous steps, the reference needs to be inside curly braces


###### responseFields
Contains fields exposed for referencing by other steps.  
**Important**: the value of the field has to be the same as set in `schemaTokenResp_MFA_Step_[number].external_name`

###### successfullResponseCode
Intermediate steps executed before obtaining final token can have http response code out of range of successful http response codes(200-299, 300-308). Set expected response code here.

###### Final token placement and format
Configuration parameters: `auth_field` and `auth_field_format` are used to point location and format of final MFA token in subsequent outgoing application requests that require authorization.

### Caching

Following adapter configuration parameters control caching of the token:
- `token_cache`, storage location, either `local` or `redis`
- `token_timeout`, in miliseconds, if set to value greater then zero then the token is refreshed every time this timeout setting elapses. Must be set to value greater then 1 minute. When `token_timeout`=0 then caching is based on expiry date obtained from schemaTokenResp_MFA_Step_[number] `expires` attribute.