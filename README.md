Itential Adapter Utilities
===

Itential Adapter Utilities is a set of Node.js runtime libraries used by Itential Adapters.

Many of the capabilities supported in Itential Adapters are built into this library so that they can e better maintained and added to adapters.

# Usage

Include `@itentialopensource/adapter-utils` as a dependency and require it:

```
const { RequestHandler, PropertyUtility } = require('@itentialopensource/adapter-utils');
```
