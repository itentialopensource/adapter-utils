/* @copyright Itential, LLC 2023 */

// Set globals
/* global log */
/* eslint global-require:warn */
/* eslint import/no-dynamic-require:warn */
/* eslint-disable consistent-return */

const aws4 = require('aws4');
const AWS = require('aws-sdk');
const querystring = require('node:querystring');

/*
 * INTERNAL FUNCTION: update device broker properties from service instance config and adapter sample properties
*/

class AuthenticationHandler {
  /**
   * Adapter Authentication Handler
   * @constructor
   */
  constructor(prongId, properties, reqH, propUtilCl) {
    this.myid = prongId;
    this.allProps = properties;
    this.requestHandlerInst = reqH;
    this.propUtil = propUtilCl;

    // set up the properties I care about
    this.refreshProperties(properties);
  }

  /**
   * refreshProperties is used to set up all of the properties for the broker handler.
   * It allows properties to be changed later by simply calling refreshProperties rather
   * than having to restart the broker handler.
   *
   * @function refreshProperties
   * @param {Object} properties - an object containing all of the properties
   */
  refreshProperties(properties) {
    const origin = `${this.myid}-authenticationHandler-refreshProperties`;
    log.trace(origin);

    if (!properties) {
      log.error(`${origin}: Authentication Handler received no properties!`);
    }
  }

  /**
   * @summary Assume an AWS STS Role
   *
   * @function assumeAWSSTSRole
   * @param {object} [options] - the AWS options
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   *
   *  @return {Object} the headers to add to the request
   */
  assumeAWSSTSRole(accessKey, secretKey, sessionT, options, STSParams, provideSign, callback) {
    const meth = 'authenticationHandler-assumeAWSRole';
    const origin = `${this.myid}-${meth}`;
    log.trace(origin);

    try {
      // set up the config object
      const configObj = {
        sessionToken: this.allProps.authentication.aws_session_token,
        accessKeyId: this.allProps.authentication.aws_access_key,
        secretAccessKey: this.allProps.authentication.aws_secret_key,
        region: options.region
      };
      // override the adapter config (ex. IAM and then Assume Role)
      if (accessKey) {
        configObj.accessKeyId = accessKey;
      }
      if (secretKey) {
        configObj.secretAccessKey = secretKey;
      }
      if (sessionT) {
        configObj.sessionToken = sessionT;
      }
      // Add optional config items (ssl, endpoint, proxy)
      if (this.allProps.authentication.aws_sts) {
        if (this.allProps.authentication.aws_sts.sslEnable === false) {
          configObj.sslEnabled = false;
        }
        if (this.allProps.authentication.aws_sts.endpoint) {
          configObj.endpoint = this.allProps.authentication.aws_sts.endpoint;
        }
        if (this.allProps.authentication.aws_sts.region) {
          configObj.region = this.allProps.authentication.aws_sts.region;
        }
        if (this.allProps.authentication.aws_sts.proxy) {
          configObj.httpOptions = {
            proxy: this.allProps.authentication.aws_sts.proxy
          };

          if (this.allProps.authentication.aws_sts.proxyagent) {
            configObj.httpOptions.agent = this.allProps.authentication.aws_sts.proxyagent;
          }
        }
      }

      // set the AWS access information (from properties)
      const sts = new AWS.STS(configObj);
      log.debug(`STS OPTIONS: ${this.propUtil.scrubSensitiveInfo(JSON.stringify(configObj))}`);

      // use STS to get the AWS access information for the user defined in STWS Params
      const stsData = {
        RoleArn: STSParams.RoleArn,
        RoleSessionName: STSParams.RoleSessionName,
        DurationSeconds: 3600
      };
      if (this.allProps.authentication.aws_sts && this.allProps.authentication.aws_sts.externalId) {
        stsData.ExternalId = this.allProps.authentication.aws_sts.externalId;
      }
      if (STSParams.ExternalId) {
        stsData.ExternalId = STSParams.ExternalId;
      }

      return sts.assumeRole(stsData, (err, data) => {
        if (err) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, `AWS Assume Role Error ${err}`, null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        if (!data || !data.Credentials || !data.Credentials.AccessKeyId || !data.Credentials.SecretAccessKey) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'AWS Assume Role did not return credentials', null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // if not providing a signature
        if (!provideSign) {
          return callback(data);
        }

        // extract the user specific info from the response
        const accessKeyId = data.Credentials.AccessKeyId;
        const secretAccessKey = data.Credentials.SecretAccessKey;
        const sessionToken = data.Credentials.SessionToken;

        // call the signature with the user specific information
        const authOpts = aws4.sign(options, { accessKeyId, secretAccessKey, sessionToken });
        if (sessionToken) {
          authOpts.headers['X-Amz-Security-Token'] = sessionToken;
        }

        // return the headers
        return callback(authOpts.headers);
      });
    } catch (e) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Caught Exception', null, null, null, e);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary Gets the hcma authorization for the call
   *
   * @function getAWSAuthorization
   * @param {string} method - the method for the action we are setting authorization for
   * @param {object} requestObj - an object that contains all of the possible parts of the request (payload, uriPathVars,
   *                              uriQuery, uriOptions and addlHeaders (optional). Can be a stringified Object.
   * @param {string} uriPath - the path for the call. (required)
   * @param {string} service - the AWS service we are talking to
   * @param {object} [stsParams] - STS Parameters to use for authentication.
   * @param {string} [roleName] - RoleName to authenticate against
   *
   *  @return {Object} the headers to add to the request
   */
  getAWSAuthorization(method, requestObj, uriPath, service, STSParams, roleName, callback) {
    const meth = 'authenticationHandler-getAWSAuthorization';
    const origin = `${this.myid}-${meth}`;
    log.trace(origin);

    try {
      // Form path
      let uriPathTranslated = '';
      if (requestObj.uriQuery && uriPath.indexOf('?') === -1) {
        const query = requestObj.uriQuery;
        uriPathTranslated = `${uriPath}?${querystring.stringify(query)}`;
      } else if (requestObj.uriQuery && uriPath.endsWith('?')) {
        const query = requestObj.uriQuery;
        uriPathTranslated = `${uriPath}${querystring.stringify(query)}`;
      } else {
        uriPathTranslated = uriPath;
      }

      // set up the options for the AWS signature
      const options = {
        host: this.allProps.host,
        method,
        path: uriPathTranslated.replace(/\/\/+/g, '/'),
        service,
        region: this.allProps.region
      };

      // add any provided headers for the call
      if (requestObj.addlHeaders) {
        options.headers = requestObj.addlHeaders;
      }
      // remove ? if there is no query and the last character is ?
      if (options.path.endsWith('?')) {
        options.path = options.path.substring(0, options.path.length - 1);
      }
      // If there is a body (POST, PATCH, PUT) add the body to the call
      if (method !== 'GET' && method !== 'DELETE') {
        if (typeof requestObj.payload === 'string') {
          options.body = requestObj.payload;
        } else {
          options.body = JSON.stringify(requestObj.payload);
        }
      }
      // override anything set in callProperties
      if (requestObj.callProperties) {
        if (requestObj.callProperties.host) {
          options.host = requestObj.callProperties.host;
        }
        if (requestObj.callProperties.region) {
          options.region = requestObj.callProperties.region;
        }
      }
      log.debug(`SIG OPTIONS: ${JSON.stringify(options)}`);

      /* BASIC STS AUTHENTICATION */
      // This will use the adapter role via the credentials provided to identify primary role and then assume the role
      // provided in the STSParams. If no adapter role credentials are provided, it will use AWS Environment
      // variables (access and secret key are null) and attempt to assume the role with those.
      // If the role assumption is successful the call is signed with the assumed role. If it fails, the call will error.
      if (STSParams && !roleName && !this.allProps.authentication.aws_iam_role) {
        log.info('Using STS for AWS Authentication');
        return this.assumeAWSSTSRole(null, null, null, options, STSParams, true, callback);
      }

      /* ADAPTER PROPERTIES AUTHENTICATION */
      // This will use the adapter role via the credentials provided to sign the call. If no adapter role credentials
      // are provided, it will use AWS Environment variables (access and secret key are null) to sign the call.
      if (!roleName && !this.allProps.authentication.aws_iam_role) {
        log.info('Using Adapter PROPERTIES for AWS Authentication');

        // call the signature with the property information
        const authOpts = aws4.sign(options, { accessKeyId: this.allProps.authentication.aws_access_key, secretAccessKey: this.allProps.authentication.aws_secret_key, sessionToken: this.allProps.authentication.aws_session_token });
        if (this.allProps.authentication.aws_session_token) {
          authOpts.headers['X-Amz-Security-Token'] = this.allProps.authentication.aws_session_token;
        }

        // return the headers
        return callback(authOpts.headers);
      }

      const myDate = new Date().getTime();
      const mySess = `${this.myid}-${myDate}`;

      /* ADAPTER PROPERTIES AUTHENTICATION WITH GLOBAL ROLE */
      // This will use the adapter role via the credentials provided to sign the call. If there is a global role for it to assume.
      // it will use AWS Environment variables (access and secret key are null) to sign the call.
      if (this.allProps.authentication.aws_access_key && this.allProps.authentication.aws_secret_key && this.allProps.authentication.aws_iam_role) {
        log.info('Using Adapter PROPERTIES for AWS Authentication to assume a global role');
        const parmas = {
          RoleArn: this.allProps.authentication.aws_iam_role,
          RoleSessionName: mySess
        };
        return this.assumeAWSSTSRole(null, null, null, options, parmas, true, callback);
      }

      /* ROLE NAME AUTHENTICATION */
      // Different scenarios to discuss here
      // 1. IAM to internal AWS Server - either Task Role (roleName) or Adapter Role (aws_iam_role)
      // 2. Adapter Role (aws_iam_role) assumes Task Role (STSParams or roleName)
      //      a. IAM to internal AWS Server for Adapter Role
      //      b. AWS STS for assuming Task Role(s) using Adapter Role
      // 3.  Pod Role assumes Adapter Role (aws_iam_role) assumes Task Role (STSParams, RoleName)
      //      a. AWS STS for assuming Adapter Role using AWS Environment (Pod Role)
      //      b. AWS STS for assuming Task Role(s) using Adapter Role
      log.info('Using roleName for AWS Authentication');

      // get the role to use for first credential call
      let myRole = roleName;
      if (this.allProps.authentication.aws_iam_role) {
        myRole = this.allProps.authentication.aws_iam_role;
      }

      // set up data for first assume role call
      const stsrole = new AWS.STS();
      const stsData = {
        RoleArn: myRole,
        RoleSessionName: mySess,
        DurationSeconds: 3600
      };

      // change role to the role name provided
      return stsrole.assumeRole(stsData, (err, data) => {
        if (err) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, `AWS Assume Role Error ${err}`, null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
        if (!data || !data.Credentials || !data.Credentials.AccessKeyId || !data.Credentials.SecretAccessKey) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'AWS Assume Role did not return credentials', null, null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        // get role keys from response so we can sign the request
        const accessKeyId = data.Credentials.AccessKeyId;
        const secretAccessKey = data.Credentials.SecretAccessKey;
        const sessionToken = data.Credentials.SessionToken;

        /* ASSUMING TASK ROLE */
        if (roleName && this.allProps.authentication.aws_iam_role) {
          stsData.RoleArn = roleName;
          log.info('Assuming Task Role after Adapter Role');
          return this.assumeAWSSTSRole(accessKeyId, secretAccessKey, sessionToken, options, stsData, true, callback);
        }
        if (STSParams) {
          log.info('Assuming Task Role (STS) after Adapter Role');
          return this.assumeAWSSTSRole(accessKeyId, secretAccessKey, sessionToken, options, STSParams, true, callback);
        }

        // sign the request
        const authOpts = aws4.sign(options, { accessKeyId, secretAccessKey, sessionToken });
        if (sessionToken) {
          authOpts.headers['X-Amz-Security-Token'] = sessionToken;
        }

        // return the headers
        return callback(authOpts.headers);
      });
    } catch (e) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Caught Exception', null, null, null, e);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }
}

module.exports = AuthenticationHandler;
