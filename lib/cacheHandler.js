/* @copyright Itential, LLC 2023 */

// Set globals
/* global log */
/* eslint global-require:warn */
/* eslint import/no-dynamic-require: warn */

const AsyncLockCl = require('async-lock');
const path = require('path');

const lock = new AsyncLockCl();
let id = null;

/**
 * @summary Creates a Cache Entity object to organize the cache by entity type.
 * @function createCacheEntity
 * @param {String} entityName - type of entity
 * @param {Array} entityList - array of data
 * @param {Object} interval - id of interval
 * @returns {Object} - created Cache Entity object
 */
function createCacheEntity(entityName, entityList, interval, sortEntities = true) {
  const origin = `${id}-cacheHandler-createCacheEntity`;
  log.trace(origin);
  return ({
    lockKey: entityName,
    entityType: entityName,
    list: entityList,
    intervalId: interval,
    sort: sortEntities
  });
}

/**
 * @summary Deletes cache data and properties. Data deletion
 * saves considerable memory.
 * @function deleteCacheData
 * @param {Object} cacheHandler - this class instance
 */
function deleteCacheData(cacheHandler) {
  const origin = `${id}-cacheHandler-deleteCacheData`;
  log.trace(origin);

  const handler = cacheHandler;

  // Log INFO level if cache is disabled
  if (!handler.enabled) {
    log.info(`${origin}: Cache is turned off.`);
  }

  // Get all keys to ensure nothing else is editing the cache at this time
  const keysArr = [];
  handler.cache.forEach((entity) => {
    keysArr.push(entity.lockKey);
  });

  lock.acquire(keysArr, () => {
    if (!handler.enabled) {
      // Clear cache and log once when going from enabled to disabled
      if (handler.cache.length > 0 || handler.propertiesMap.size > 0) {
        log.info(`${origin}: Cache has been cleared.`);
      }

      handler.cache.forEach((entity) => {
        clearInterval(entity.intervalId);
      });
      handler.cache = [];
      handler.propertiesMap.clear();
    } else {
      log.debug(`${origin}: Cache is currently enabled.`);
    }
  });
}

/**
 * @summary changes the frequency of which to update an entity type
 * @function changeUpdateFrequency
 * @param {Array of Objects} currentCache - cache
 * @param {String} entityType - entity type of which to update
 * @param {Integer} newFrequency - how often to updaate (in minutes)
 * @param {cacheHandler} cacheHandler - instance of class to edit
 */
function changeUpdateFrequency(currentCache, entityType, newFrequency, cacheHandler) {
  const origin = `${id}-cacheHandler-changeUpdateFrequency`;
  log.trace(origin);

  const cache = currentCache;
  for (let i = 0; i < cache.length; i += 1) {
    if (cache[i].entityType === entityType) {
      cacheHandler.populateCache(entityType);
      clearInterval(cache[i].intervalId);
      cache[i].intervalId = setInterval(() => {
        cacheHandler.populateCache(entityType);
      }, newFrequency * 60000);
      log.info(`Changed update frequency for ${entityType}.`);
      return;
    }
  }
  log.error(`Failed to change interval. ${entityType} does not exist in cache.`);
}

/**
 * @summary Given an array of populate objects from a single entityType, initialize missing fields.
 * @function validatePopulate
 * @param {Array} populateArr - array of populate objects from properties
 */
function validatePopulate(populateArr) {
  const origin = `${id}-cacheHandler-validatePopulate`;
  log.trace(origin);

  const arr = populateArr;
  for (let i = 0; i < arr.length; i += 1) {
    if (!arr[i].path) {
      log.error(`No path in populate in properties for cache! Path is ${path}`);
      // let brokerHandler handle this
    } else {
      arr[i].method = arr[i].method || 'GET';
      arr[i].query = arr[i].query || {};
      arr[i].body = arr[i].body || {};
      arr[i].headers = arr[i].headers || {};
      arr[i].handleFailure = arr[i].handleFailure || 'ignore';
      arr[i].requestFields = arr[i].requestFields || {};
      arr[i].responseDatakey = arr[i].responseDatakey || '';
      arr[i].responseFields = arr[i].responseFields || {};
    }
  }
}

/**
 * @summary Checks the properties passed in for the cache are valid,
 * giving them default values if possible and removing them otherwise.
 * @function validateProperties
 * @param {Object} obj - properties json object
 * @returns - adjusted valid properties with defaults
 */
function validateProperties(obj) {
  const origin = `${id}-cacheHandler-validateProperties`;
  log.trace(origin);

  if (obj === null) {
    log.error(`${origin}: Null properties!`);
    throw new Error('Null properties!');
  }

  const props = obj;

  // Check cache, cache.enabled, cache.entities exist
  if (!props.cache) {
    // No cache, initialize default props
    props.cache = {
      enabled: false,
      entities: []
    };
  } else {
    if (!props.cache.enabled) {
      log.info(`${origin}: Cache is turned off.`);
      props.cache.enabled = false;
    }

    if (props.cache.enabled && !props.cache.entities) {
      props.cache.entities = [];
      log.info(`${origin}: Cache entities initialized as an empty array.`);
    }
  }

  // Validate each entity in cache.entities if cache is enabled
  if (props.cache.enabled) {
    for (let i = 0; i < props.cache.entities.length; i += 1) {
      const entity = props.cache.entities[i];

      // Ensure entityType and populate exist and are valid
      if (!entity.entityType || !entity.populate || entity.populate.length === 0) {
        log.warn(`${origin}: Removed invalid cache property at index ${i}`);
        props.cache.entities.splice(i, 1);
        i -= 1; // Adjust index after removal
        break;
      }

      // Validate frequency (default: 24 hrs, min: 15 mins, max: 1 week)
      if (!entity.frequency) {
        entity.frequency = 24 * 60; // Default to 1 day in minutes
        log.debug(`${origin}: Frequency set to default (1440 minutes).`);
      } else if (entity.frequency < 15) {
        log.warn(`${origin}: Frequency ${entity.frequency} too small! Adjusted to 15 minutes.`);
        entity.frequency = 15;
      } else if (entity.frequency > 10080) {
        log.warn(`${origin}: Frequency ${entity.frequency} too large! Adjusted to 1 week.`);
        entity.frequency = 10080;
      }

      // Default flushOnFail = false
      if (entity.flushOnFail === undefined) {
        entity.flushOnFail = false;
        log.debug(`${origin}: flushOnFail defaulted to false.`);
      }

      // Default limit = 1000
      if (!entity.limit) {
        entity.limit = 1000;
        log.debug(`${origin}: Limit defaulted to 1000.`);
      }

      // Default retryAttempts = 5
      if (!entity.retryAttempts) {
        entity.retryAttempts = 5; // Used for startup retries
        log.debug(`${origin}: retryAttempts defaulted to 5.`);
      }

      // Validate cachedTasks
      if (!entity.cachedTasks) {
        entity.cachedTasks = [];
        log.debug(`${origin}: cachedTasks initialized as an empty array.`);
      } else {
        entity.cachedTasks.forEach((cacheTask, idx) => {
          const curTask = cacheTask; // for lint
          if (!cacheTask.name) {
            log.error(`${origin}: Cached task at index ${idx} has no name! Defaulting to an empty string.`);
            curTask.name = ''; // Prevent errors, will not match a task name
          }
          if (!cacheTask.filterField) {
            curTask.filterField = ''; // Assume a "get all" call if no filter specified
          }
        });
      }

      // Fill in missing populate fields
      validatePopulate(entity.populate);
    }
  }

  return props;
}

/**
 * @summary Compares two objects by name for sorting purposes.
 * @function compareByName
 * @param {Object} a - object to compare
 * @param {Object} b - object to compare
 * @returns {int} - specifying data order in relation to each other
 */
function compareByName(a, b) {
  if (a.name > b.name) {
    return 1;
  }
  if (a.name < b.name) {
    return -1;
  }
  return 0;
}

/**
 * @summary tests if object is an options object. Defaults:
 *  - filter = ""
 *  - start = 0 and limit = MAX_SAFE_INTEGER
 *  - sort = false
 * @function validateOptions
 * @param {Object} obj - object to be tested
 * @return {Object} - modified object with defaults
 */
function validateOptions(obj) {
  const origin = `${id}-cacheHandler-validateOptions`;
  log.trace(origin);

  const opt = obj || {};

  const keys = Object.keys(opt);
  if (!keys.includes('filter')) {
    opt.filter = {}; // filter for nothing means get everything
  }
  if (!keys.includes('start') && !keys.includes('limit')) {
    opt.start = 0;
    opt.limit = Number.MAX_SAFE_INTEGER; // return everything, maxed at max safe integer
  } else if (!(keys.includes('start') && keys.includes('limit')) || opt.start < 0 || opt.limit < 0) {
    log.error(`${origin}: incomplete or invalid options with start and limit!`);
    throw new Error('Invalid options on accessing cache');
  }

  return opt;
}

/**
 * @summary Helper method that retrieves entities from the cache
 * @function retrieveCacheEntriesHelper
 *
 * @param {Object} cache - cache to search through
 * @param {String} entityType - entity type to retrieve
 * @param {Object} options - options on how to filter and return the data
 */
function retrieveCacheEntriesHelper(cache, entityType, options, callback) {
  // go through the cache to find the entity we care about
  for (let i = 0; i < cache.length; i += 1) {
    // check if entity type we care about
    if (cache[i].entityType === entityType) {
      // Lock the cache for the entity so we can take what is in the cache
      // Lock and instant unlock as wait until lock is free
      return lock.acquire(cache[i].lockKey, (done) => {
        // return the entity's list and free the lock
        done(cache[i].list);
      }, (ret) => {
        let arr = [];

        // populate re-attempt fails
        if (!ret) {
          log.error(`${origin}: No list in retrieve helper, re-attempt populate failed!`);
          return callback(null, 'No list in retrieve cache helper!');
        }

        // no cache data found
        if (ret.length === 0) {
          log.info('Cache data retrieved and found to be empty.');
          return callback(arr);
        }

        // apply filter
        if (!options.filter || Object.keys(options.filter).length === 0) {
          // no filter, get everything
          log.info('No filter applied to cache retrieved');
          arr = ret;
        } else {
          // get cache data with substring in name
          const filterField = Object.keys(options.filter)[0];
          const filter = options.filter[filterField];
          for (let j = 0; j < ret.length; j += 1) {
            if (ret[j][filterField].includes(filter)) {
              arr.push(ret[j]);
            }
          }
        }

        // don't need to sort if we know it's OOB
        if (options.limit * options.start >= arr.length) {
          log.warn(`${options.start} is not a valid page, too large!`);
          return callback([]);
        }

        // Pagination is not needed when cache is enabled
        // pagination: get entities limit*start to limit*(start+1) - 1
        // if (options.start >= 0 && options.limit > 0) { // Probably should handle this in properties.
        //   // if last "page" then will be returning fewer items
        //   const end = Math.min(arr.length, options.limit * (options.start + 1));
        //   arr = arr.slice(options.limit * options.start, end);
        // }
        return callback(arr); // for linting
      });
    }
  }
  log.error(`${origin}: No entityType ${entityType} in retrieve helper!`);
  return callback(null, `No ${entityType} in retrieve cache helper!`); // error not found
}

/**
 * @summary Removes all data of a certain entity type
 * @function removeCacheEntry
 * @param {Array} cache - the cache to remove from
 * @param {String} entityType - the entity type of which to remove
 */
function removeCacheEntry(cache, entityType) {
  const origin = `${id}-cacheHandler-removeCacheEntry`;
  log.trace(origin);

  for (let i = 0; i < cache.length; i += 1) {
    if (cache[i].entityType === entityType) {
      log.debug(`${origin}: Found entity type '${entityType}' in cache. Attempting to remove.`);

      lock.acquire(cache[i].lockKey, () => {
        log.debug(`${origin}: Lock acquired for entity type '${entityType}'. Clearing interval and removing from cache.`);
        clearInterval(cache[i].intervalId);
        cache.splice(i, 1);
      });

      log.info(`${origin}: Successfully removed '${entityType}' from cache.`);
      return;
    }
  }

  log.error(`${origin}: Entity type '${entityType}' not found in cache. Removal failed.`);
}

/**
 * @summary makes IAP Calls through Generic Handler
 *
 * @function makeIAPCall
 * @param calls - calls to make
 * @param requestHandler - instance of requestHandler to make call
 * @param callback - data or error
 */
function makeIAPCall(calls, requestHandler, callback) {
  const callPromises = [];

  for (let i = 0; i < calls.length; i += 1) {
    log.debug('Response :', calls[i].responseFields);
    callPromises.push(new Promise((resolve, reject) => {
      const metadata = {};
      if (calls[i].pagination) {
        metadata.pagination = calls[i].pagination;
        metadata.pagination.responseDatakey = calls[i].responseDatakey || '';
      }
      requestHandler.iapMakeGenericCall(metadata, calls[i].path, calls[i], [{ fake: 'fakedata' }], [], (callRet, callErr) => {
        if (callErr) {
          log.error('Make iap call failed with error');
          log.error(callErr);
          if (callErr.icode === 'AD.301') {
            log.error(callErr.IAPerror.displayString);
            return reject(callErr);
          }
          log.warn(`Call failed for path ${calls[i].path} with error code ${callErr.icode}. Rejecting.`);
          return reject(callErr);
        }

        // callRet is the object returned from that call
        log.info(`Sucessful Call. Adding to cache from path ${calls[i].path}`);
        return resolve(callRet);
      });
    }));
  }

  let returnData = [];
  // Reworking the brokerHandler calls
  return Promise.all(callPromises).then((results) => { // array of each Promise result
    for (let i = 0; i < results.length; i += 1) {
      returnData = returnData.concat(results[i]);
    }
    callback(returnData, null); // Returns data if all calls succeed
  }, (error) => {
    callback(null, error); // Throws error if at least one call failed
  });
}

/**
 * @summary Helper method that tries to populate the cache after 5 seconds
 *
 * @param {String} entityType - entity type to populate
 * @param {CacheHandler} cacheHandler - instance of cacheHandler
 * @param {Integer} attemptsRemaining - amount of attempts left to retry
 * @param {function} callback - returns whether or not that cache was successfully populated
 */
function populateTimeout(entityType, cacheHandler, attemptsRemaining, callback) {
  let ar = attemptsRemaining;
  log.info(`Retrying to populate cache for ${entityType}. Attempts Remaining: ${ar}`);
  setTimeout(() => {
    cacheHandler.populateCache(entityType).then((result) => {
      if (result && result[0] === 'success') {
        return callback(true);
      }
      ar -= 1;
      if (ar === 0) {
        return callback(false);
      }
      return populateTimeout(entityType, cacheHandler, ar, callback);
    });
  }, 10000);
}

/**
 * @summary tries to populate the cache again after populate fails on startup
 *
 * @param {String} entityType - entity type to populate
 * @param {CacheHandler} cacheHandler - instance of CacheHandler
 * @param {Integer} attempts - amount of attempts to retry
 */
async function retryPopulate(entityType, cacheHandler, attempts) {
  log.info('Retrying populate...');
  return new Promise((resolve) => {
    populateTimeout(entityType, cacheHandler, attempts, (success) => {
      if (success) {
        return resolve(`Cache updated for ${entityType}.`);
      }
      return resolve(`Populate cache for ${entityType} failed.`);
    });
  });
}

// Exposed handler class
class CacheHandler {
  /**
   * Adapter Cache Handler
   * @constructor
   */
  constructor(prongId, properties, directory, reqH) {
    id = prongId; // accessable to non-exposed methods
    this.baseDir = directory;
    this.requestHandler = reqH; // Request Handler object with id, props, dir

    this.cache = []; // array of CacheEntity objects

    // set up the properties I care about
    this.propertiesMap = new Map(); // entityType->properties map
    this.refreshProperties(properties);
  }

  /**
   * refreshProperties is used to set up all of the properties for the cache handler.
   * It allows properties to be changed later by simply calling refreshProperties rather
   * than having to restart the cache handler.
   *
   * @function refreshProperties
   * @param {Object} properties - an object containing all of the properties
   */
  refreshProperties(properties) {
    const origin = `${id}-cacheHandler-refreshProperties`;
    log.trace(origin);

    if (properties === null) {
      log.error(`${origin}: Cache Handler received no properties!`);
      return;
    }

    log.debug(`${origin}: Validating and setting new properties.`);
    this.props = validateProperties(properties);

    const wasEnabled = this.enabled || false; // in case undefined
    this.enabled = this.props.cache.enabled;

    if (!this.enabled) {
      if (wasEnabled) {
        log.warn(`${origin}: Cache was disabled from enabled state. Clearing cache for memory efficiency.`);
        deleteCacheData(this);
      } else {
        log.debug(`${origin}: Cache remains disabled. No action required.`);
      }
      return;
    }

    log.debug(`${origin}: Checking for deleted or updated entity types.`);
    this.propertiesMap.forEach((value, key) => {
      let entityExists = false;

      for (let i = 0; i < this.props.cache.entities.length; i += 1) {
        if (this.props.cache.entities[i].name === key) {
          entityExists = true;
          break;
        }
      }

      if (!entityExists) {
        log.warn(`${origin}: Detected removed entity type '${key}'. Removing from cache.`);
        removeCacheEntry(this.cache, key);
        this.propertiesMap.delete(key);
        log.info(`${origin}: Deleted properties for entity type '${key}' with value '${value}'.`);
      }
    });

    log.debug(`${origin}: Updating and setting properties map for cache entities.`);
    this.props.cache.entities.forEach((entity) => {
      if (!this.propertiesMap.has(entity.entityType)) {
        log.debug(`${origin}: Adding new entity type '${entity.entityType}' to properties map.`);
        this.propertiesMap.set(entity.entityType, {});
        const entityProps = this.propertiesMap.get(entity.entityType);
        entityProps.frequency = entity.frequency;
        entityProps.flushOnFail = entity.flushOnFail;
        entityProps.populate = entity.populate;
        entityProps.limit = entity.limit;
        entityProps.retryAttempts = entity.retryAttempts;

        const newIntervalId = setInterval(() => {
          log.debug(`${origin}: Populating cache for entity type '${entity.entityType}'.`);
          this.populateCache(entity.entityType);
        }, entity.frequency * 60000);

        const sort = Object.prototype.hasOwnProperty.call(entity, 'sort') ? entity.sort : true; // default true
        this.cache.push(createCacheEntity(entity.entityType, null, newIntervalId, sort));

        this.populateCache(entity.entityType)
          .then((result) => {
            if (result && result[0] === 'error') {
              log.warn(`${origin}: Populate failed for '${entity.entityType}'. Retrying...`);
              retryPopulate(entity.entityType, this, entity.retryAttempts).then((nextResult) => {
                log.info(`${origin}: Retry result for '${entity.entityType}': ${nextResult}`);
              });
            }
          })
          .catch((error) => {
            log.error(`${origin}: Populate cache failed for '${entity.entityType}'.`);
            if (error.icode === 'AD.301') {
              log.error(`${origin}: Clearing interval and removing cache entity '${entity.entityType}' due to error.`);
              clearInterval(newIntervalId);
              this.cache.removeCacheEntry(this.cache, entity.entityType);
            }
          });
      } else {
        log.debug(`${origin}: Updating frequency and properties for existing entity type '${entity.entityType}'.`);
        const entityProps = this.propertiesMap.get(entity.entityType);

        if (entity.frequency !== entityProps.frequency) {
          log.info(`${origin}: Updating update frequency for '${entity.entityType}' from '${entityProps.frequency}' to '${entity.frequency}'.`);
          changeUpdateFrequency(this.cache, entity.entityType, entity.frequency, this);
        }

        entityProps.frequency = entity.frequency;
        entityProps.flushOnFail = entity.flushOnFail;
        entityProps.populate = entity.populate;
        entityProps.limit = entity.limit;
        entityProps.retryAttempts = entity.retryAttempts;
      }
    });
  }

  /**
   * @summary Populates/updates cache with entities' data from IAP call
   * @function populateCache
   * @param {String/Array of Strings} entities - entities of which we want to store
   * @param {Boolean} onlyIfNecessary - If true and cache has been populated after acquiring the lock, does nothing
   * @return {Array of Strings} - whether each entity succeeded or errored
   */
  async populateCache(entities) {
    const origin = `${id}-cacheHandler-populateCache`;
    log.trace(origin);

    if (!this.enabled) {
      log.info(`${origin}: Cache is not enabled. Populate operation aborted.`);
      return Promise.reject(new Error('Cache is not enabled!'));
    }

    log.info(`${origin}: Starting cache population.`);

    // Support string and array input
    let entityArr = entities;
    if (!Array.isArray(entities)) {
      entityArr = [entities];
    }

    // Ensure all entities are tracked
    for (let i = 0; i < entityArr.length; i += 1) {
      const entityType = entityArr[i];
      if (!this.propertiesMap.has(entityType)) {
        log.error(`${origin}: ${entityType} is an untracked entity type! Check properties.`);
        return Promise.reject(new Error(`${entityType} is an untracked entity type! Check properties.`));
      }
    }

    try {
      const promiseArr = [];
      // Populate each entity type
      for (let j = 0; j < entityArr.length; j += 1) {
        const entityType = entityArr[j];
        for (let i = 0; i < this.cache.length; i += 1) {
          if (this.cache[i].entityType === entityType) {
            promiseArr.push(new Promise((resolve, reject) => {
              makeIAPCall(this.propertiesMap.get(entityType).populate, this.requestHandler, (cacheData, error) => {
                // Error handling
                let errorMsg = null;
                if (error) {
                  errorMsg = `${origin}: ${entityType} failed with error ${error.IAPerror.displayString}.`;
                } else if (cacheData.length > this.propertiesMap.get(entityType).limit) {
                  errorMsg = `${origin}: ${entityType} failed. Data surpassed the limit.`;
                } else if (cacheData.length === 0) {
                  errorMsg = `${origin}: ${entityType} failed. Nothing found.`;
                }

                if (errorMsg !== null) {
                  if (this.propertiesMap.get(entityType).flushOnFail) {
                    lock.acquire(this.cache[i].lockKey, (done) => {
                      this.cache[i].list = null;
                      done(`${errorMsg} Cache flushed.`);
                    }, (ret) => {
                      log.error(ret);
                    });
                  } else {
                    log.error(`${errorMsg} Keeping old data.`);
                  }

                  if (error && error.icode === 'AD.301') {
                    return reject(error);
                  }
                  return resolve('error');
                }

                // Sort and update cache
                if (this.cache[i].sort) {
                  cacheData.sort(compareByName);
                }
                lock.acquire(this.cache[i].lockKey, (done) => {
                  if (this.enabled) {
                    this.cache[i].list = cacheData;
                    done(`${origin}: ${entityType} cache updated successfully.`);
                  } else {
                    done(`${origin}: Populate cancelled due to disabled cache.`);
                  }
                }, (ret) => {
                  log.info(ret);
                });
                return resolve('success');
              });
            }));
            break;
          }
        }
      }

      // Resolve all promises
      const arr = await Promise.allSettled(promiseArr);
      return new Promise((resolve, reject) => {
        const valueArray = [];
        for (let p = 0; p < arr.length; p += 1) {
          if (arr[p].status === 'rejected') {
            reject(arr[p].reason);
          }
          valueArray.push(arr[p].value);
        }
        resolve(valueArray);
      });
    } catch (e) {
      log.error(`${origin}: An exception occurred during cache population - ${e.message}`);
      throw new Error(e);
    }
  }

  /**
   * @summary Retrieves an entity's data from cache under specified filters
   * @function retrieveCacheEntries
   * @param {String} entityType - entity type that we want to retrieve items from
   * @param {Object} opts - Options of what we want to search for and return
   *                           Keys:
   *                              Filter (String) - Substring in which to see which's data
   *                                                names contain
   *                              Start (int) - Pagination Number (Not sure how to implement)
   *                              Limit (int) - limit of how much data to send back
   *                              Sort (Boolean) - whether we sort the data or not
   * @return {Array} - array of data objects
   */
  retrieveCacheEntries(entityType, opts, callback) {
    const origin = `${id}-cacheHandler-retrieveCacheEntries`;
    log.trace(origin);

    if (!this.enabled) {
      log.info(`${origin}: Cache is not enabled!`);
      return callback(null, 'Cache is not enabled.');
    }

    if (typeof entityType !== 'string') {
      log.error(`${origin}: Invalid entityType - ${entityType} is not of type String.`);
      return callback(null, `${entityType} is not of type String`);
    }

    try {
      const options = validateOptions(opts); // may throw error

      log.debug(`${origin}: Searching cache for entityType: ${entityType}.`);
      // Search for the specified entityType in the cache
      for (let i = 0; i < this.cache.length; i += 1) {
        if (this.cache[i].entityType === entityType) {
          log.info(`${origin}: Found cache entry for entityType: ${entityType}.`);

          // Lock the cache entry for safe access
          return lock.acquire(this.cache[i].lockKey, (done) => {
            log.debug(`${origin}: Retrieved cache list for entityType: ${entityType}.`);
            done(this.cache[i].list);
          }, (ret) => {
            if (!ret) {
              log.warn(`${origin}: Last IAP call failed and flushed cache for ${entityType}. Attempting to repopulate.`);

              // Repopulate cache and retry retrieval
              return this.populateCache(entityType).then((result) => {
                if (result && result[0] === 'success') {
                  log.info(`${origin}: Repopulation succeeded for ${entityType}. Retrieving updated entries.`);
                  return retrieveCacheEntriesHelper(this.cache, entityType, options, callback);
                }
                log.error(`${origin}: Repopulation failed for ${entityType}.`);
                return callback(null, `Retrieve call for ${entityType} failed, and populate re-attempt failed.`);
              }).catch((err) => {
                log.error(`${origin}: Error during repopulation for ${entityType} - ${err.message}`);
                return callback(null, `Retrieve call for ${entityType} failed due to an error during repopulation.`);
              });
            }

            log.debug(`${origin}: Retrieved entries successfully for entityType: ${entityType}.`);
            return retrieveCacheEntriesHelper(this.cache, entityType, options, callback);
          });
        }
      }

      log.error(`${origin}: Cache entry not found for entityType: ${entityType}.`);
      return callback(null, `Retrieve call for ${entityType} failed.`);
    } catch (e) {
      log.error(`${origin}: Exception occurred during retrieval - ${e.message}`);
      throw new Error(e);
    }
  }

  /**
   * @summary Method to check if the entity is in the cache. Faster than
   * retrieveCacheEntries hence the seperation
   *
   * @function isEntityCached
   * @param {String} entityType - the entity type to check for
   * @param {Array of String} entityNames - the specific entities we are looking for of that type
   *
   * @return {Array of Boolean} - whether associated entity is found
   */
  isEntityCached(entityType, entityNames, callback) {
    const origin = `${id}-cacheHandler-isEntityCached`;
    log.trace(origin);

    if (!this.enabled) {
      log.info(`${origin}: Cache is not enabled!`);
      return callback(null, 'Cache is not enabled!');
    }

    const names = Array.isArray(entityNames) ? entityNames : [entityNames];

    try {
      log.debug(`${origin}: Checking cache for entityType: ${entityType}, entityNames: ${names}.`);

      for (let i = 0; i < this.cache.length; i += 1) {
        if (this.cache[i].entityType === entityType) {
          log.info(`${origin}: Cache entry found for entityType: ${entityType}.`);

          const returnVals = [];

          // Lock the cache entry to safely access the list
          return lock.acquire(this.cache[i].lockKey, (done) => {
            log.debug(`${origin}: Acquired lock for cache entry of entityType: ${entityType}.`);
            done(this.cache[i].list);
          }, (ret) => {
            if (!ret) {
              log.warn(`${origin}: Cache list is null or empty for ${entityType}. Attempting to repopulate.`);

              return this.populateCache(entityType).then((result) => {
                if (result && result[0] === 'success') {
                  log.info(`${origin}: Cache repopulation succeeded for ${entityType}. Rechecking cache.`);

                  // Reacquire lock to access the updated cache list
                  return lock.acquire(this.cache[i].lockKey, (done) => {
                    names.forEach((name) => {
                      const isFound = this.cache[i].list.some((item) => item.name === name);
                      returnVals.push(isFound ? 'found' : 'notfound');
                    });
                    done(returnVals);
                  }, (retVals) => {
                    log.debug(`${origin}: Final cache check results: ${retVals}`);
                    return callback(retVals);
                  });
                }

                log.error(`${origin}: Cache repopulation failed for ${entityType}.`);
                return callback(null, `isEntityCached call for ${entityType} failed.`);
              }).catch((err) => {
                log.error(`${origin}: Error during cache repopulation for ${entityType} - ${err.message}`);
                return callback(null, `isEntityCached call for ${entityType} failed due to repopulation error.`);
              });
            }

            log.debug(`${origin}: Processing cached list for entityType: ${entityType}.`);
            names.forEach((curName) => {
              const isFound = ret.some((item) => item.name === curName);
              returnVals.push(isFound ? 'found' : 'notfound');
            });

            log.debug(`${origin}: Cache check results: ${returnVals}`);
            return callback(returnVals);
          }); // end of lock acquire
        }
      }

      log.error(`${origin}: No cache entry found for entityType: ${entityType}.`);
      return callback(false);
    } catch (e) {
      log.error(`${origin}: Exception occurred during cache check - ${e.message}`);
      return callback(false);
    }
  }

  /**
   * @function isEntityTypeToBeCached
   * @summary Checks whether the cache properties for that entity type
   * is set up, meaning that the entity type should be cached
   * @param {String} entityType - the entity type to check for
   * @returns {Boolean} - whether the entity type is set to be cached
   */
  isEntityTypeToBeCached(entityType, callback) {
    const origin = `${id}-cacheHandler-isEntityTypeToBeCached`;
    log.trace(`${origin}: Checking if entityType should be cached.`);

    if (!this.enabled) {
      log.info(`${origin}: Cache is not enabled.`);
      return callback(null, 'Cache is not enabled!');
    }

    const isCached = this.propertiesMap.has(entityType);
    log.debug(`${origin}: entityType "${entityType}" is ${isCached ? '' : 'not '}to be cached.`);
    return callback(isCached);
  }

  /**
   * @function isTaskCached
   * @summary Checks whether the task should be cached, for adapter
   * (non-broker level) calls
   * @param {String} entityType - entity type from identifyRequest
   * @param {String} task - method name
   */
  isTaskCached(entityType, task) {
    const origin = `${id}-cacheHandler-isTaskCached`;
    log.trace(`${origin}: Checking if task "${task}" is cached under entityType "${entityType}".`);

    // If cache is disabled or the entityType is not tracked, return false early
    if (!this.enabled) {
      log.info(`${origin}: Cache is not enabled.`);
      return false;
    }

    if (!this.propertiesMap.has(entityType)) {
      log.warn(`${origin}: entityType "${entityType}" is not configured in propertiesMap.`);
      return false;
    }

    const cachedEntity = this.props.cache.entities.find(
      (entity) => entity.entityType === entityType
    );

    if (!cachedEntity) {
      log.error(`${origin}: entityType "${entityType}" not found in cache.`);
      return false;
    }

    const isTaskFound = cachedEntity.cachedTasks.some(
      (cachedTask) => cachedTask.name === task
    );

    if (isTaskFound) {
      log.debug(`${origin}: Task "${task}" is cached under entityType "${entityType}".`);
      return true;
    }

    log.debug(`${origin}: Task "${task}" is not cached under entityType "${entityType}".`);
    return false;
  }
}

module.exports = CacheHandler;
