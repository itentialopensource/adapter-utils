/* @copyright Itential, LLC 2023 */

// Set globals
/* global log */
/* eslint consistent-return: warn */

/* NodeJS internal utilities */
const jsonQuery = require('json-query');

// INTERNAL FUNCTIONS

/**
 * @summary Update the offset for paginated call.
 * @function incrementOffset
 * @param {String} offsetType - type of offset (page or limit)
 * @param {Number} data - previous offset
 * @param {Number} limit - call limit
 * @returns {Number} - new offset
 */
function incrementOffset(offsetType, previousOffset, limit) {
  let newOffset;
  if (offsetType === 'limit') {
    newOffset = previousOffset + limit;
  } else if (offsetType === 'page') {
    newOffset = previousOffset + 1;
  } else {
    throw new Error(`Offset Type : ${offsetType} not supported or insufficient data was provided`);
  }
  return newOffset;
}

/**
 * @summary Function to set the value at a specified property path
 * @function setNestedProperty
 * @param {Object} oldValue - Object to update
 * @param {String} responseDataKey - path to property to update in . separated string
 * @param {Object} newValue - Object with data to use in update
 */
function setNestedProperty(oldValue, responseDatakey, newValue) {
  const newRes = jsonQuery(responseDatakey, { data: newValue.response }).value;
  const oldRes = jsonQuery(responseDatakey, { data: oldValue.response }).value;
  const path = responseDatakey.split('.');
  let currentObj = oldValue.response;
  for (let i = 0; i < path.length - 1; i += 1) {
    const segment = path[i];
    if (Object.hasOwnProperty.call(currentObj, segment)) {
      currentObj = currentObj[segment];
    } else {
      currentObj[segment] = {};
      currentObj = currentObj[segment];
    }
  }
  // Modifies value at path and updates original object
  currentObj[path[path.length - 1]] = oldRes.concat(newRes);
}

class GenericHandler {
  /**
   * Adapter Generic Handler
   * @constructor
   */
  constructor(prongId, properties, reqH) {
    this.myid = prongId;
    this.allProps = properties;
    this.requestHandlerInst = reqH;

    // set up the properties I care about
    this.refreshProperties(properties);
  }

  /**
   * refreshProperties is used to set up all of the properties for the broker handler.
   * It allows properties to be changed later by simply calling refreshProperties rather
   * than having to restart the broker handler.
   *
   * @function refreshProperties
   * @param {Object} properties - an object containing all of the properties
   */
  refreshProperties(properties) {
    const origin = `${this.myid}-genericHandler-refreshProperties`;
    log.trace(origin);

    if (!properties) {
      log.error(`${origin}: Generic Handler received no properties!`);
    }
  }

  /**
   * Makes the requested generic call
   *
   * @function expandedGenericAdapterRequest
   * @param {object} metadata - metadata for the call (optional).
   * @param {string} uriPath - the path of the api call - do not include the host, port, base path or version (optional)
   * @param {string} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (optional)
   * @param {object} pathVars - the parameters to be put within the url path (optional).
   * @param {object} queryData - the parameters to be put on the url (optional).
   * @param {object} requestBody - the body to add to the request (optional).
   * @param {object} addlHeaders - additional headers to be put on the call (optional).
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  expandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback) {
    const meth = 'genericHandler-expandedGenericAdapterRequest';
    const origin = `${this.myid}-${meth}`;
    log.trace(origin);

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (uriPath === undefined || uriPath === null || uriPath === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Missing Data', ['uriPath'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (restMethod === undefined || restMethod === null || restMethod === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Missing Data', ['restMethod'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    // Need to add dynamic/runtime path values here
    if (pathVars) {
      const pathKeys = Object.keys(pathVars);

      // replace each key found in the path with its value
      for (let i = 0; i < pathKeys.length; i += 1) {
        uriPath.replace(`{${pathKeys[i]}}`, pathVars[pathKeys[i]]);
      }
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    // remove any leading / and split the uripath into path variables
    let myPath = uriPath;
    while (myPath.indexOf('/') === 0) {
      myPath = myPath.substring(1);
    }
    const pathComp = myPath.split('/');
    const queryParamsAvailable = queryData;
    const queryParams = {};
    const bodyVars = requestBody;

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
          && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathComp,
      uriQuery: queryParams,
      uriOptions: {}
    };
    // add headers if provided
    if (addlHeaders) {
      reqObj.addlHeaders = addlHeaders;
    }
    if (metadata && metadata.datatype) {
      reqObj.datatype = metadata.datatype;
    }
    if (metadata && metadata.authData) {
      reqObj.authData = metadata.authData;
    }
    if (metadata && metadata.callProperties) {
      reqObj.callProperties = metadata.callProperties;
    }

    // determine the call and return flag
    let action = 'getGenerics';
    let returnF = true;
    if (restMethod.toUpperCase() === 'POST') {
      action = 'createGeneric';
    } else if (restMethod.toUpperCase() === 'PUT') {
      action = 'updateGeneric';
    } else if (restMethod.toUpperCase() === 'PATCH') {
      action = 'patchGeneric';
    } else if (restMethod.toUpperCase() === 'DELETE') {
      action = 'deleteGeneric';
      returnF = false;
    }

    // Change the call and return flag if no base path
    if (metadata && metadata.basepath && metadata.basepath.toUpperCase() === 'NOBASE') {
      action = 'getGenericsNoBase';
      returnF = true;
      if (restMethod.toUpperCase() === 'POST') {
        action = 'createGenericNoBase';
      } else if (restMethod.toUpperCase() === 'PUT') {
        action = 'updateGenericNoBase';
      } else if (restMethod.toUpperCase() === 'PATCH') {
        action = 'patchGenericNoBase';
      } else if (restMethod.toUpperCase() === 'DELETE') {
        action = 'deleteGenericNoBase';
        returnF = false;
      }
    }

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      // See if AWS call requiring authentication
      if (metadata && metadata.service) {
        return this.requestHandlerInst.getAWSAuthorization(restMethod, reqObj, uriPath, metadata.service, null, null, (signature, authError) => {
          if (authError) {
            return callback(null, authError);
          }
          reqObj.addlHeaders = { ...reqObj.addlHeaders, ...signature };
          this.requestHandlerInst.identifyRequest('.generic', action, reqObj, returnF, (irReturnData, irReturnError) => {
            // if we received an error or there is no response on the results
            // return an error
            if (irReturnError) {
              /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
              return callback(null, irReturnError);
            }
            if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
              const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Invalid Response', ['genericAdapterRequest'], null, null, null);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
            // return the response
            return callback(irReturnData, null);
          });
        });
      }

      // Does not support AWS adapters due to the auth call above
      if (metadata && metadata.pagination && metadata.pagination.requestLocation && metadata.pagination.limitVar && metadata.pagination.offsetVar && metadata.pagination.incrementBy) {
        return this.expandedGenericAdapterRequestPaginated(metadata.pagination, action, reqObj, returnF, meth, (returnData, returnError) => {
          if (returnError) {
            /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
            return callback(null, returnError);
          }
          if (!Object.hasOwnProperty.call(returnData, 'response')) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Invalid Response', ['genericAdapterRequest'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }
          /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
          // return the response
          return callback(returnData, null);
        });
      }
      return this.requestHandlerInst.identifyRequest('.generic', action, reqObj, returnF, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Invalid Response', ['genericAdapterRequest'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * Makes the requested generic call
   *
   * @function expandedGenericAdapterRequestPaginated
   * @param {Object} metadata - metadata for the call (optional).
   *                 Can be a stringified Object.
   * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (optional)
   * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (optional)
   * @param {Object} pathVars - the parameters to be put within the url path (optional).
   *                 Can be a stringified Object.
   * @param {Object} queryData - the parameters to be put on the url (optional).
   *                 Can be a stringified Object.
   * @param {Object} requestBody - the body to add to the request (optional).
   *                 Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {Object} paginationObject - object specifying pagination variables and increment method
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  expandedGenericAdapterRequestPaginated(paginationObject, action, reqObj, returnF, meth, callback) {
    const origin = `${this.myid}-requestHandler-expandedGenericAdapterRequestPaginated`;
    log.trace(origin);
    let results;
    // Set up variables for calls
    const { offsetVar } = paginationObject;
    const pagType = paginationObject.requestLocation; // Body or query supported
    let pagLocation;
    if (pagType === 'body') {
      pagLocation = 'payload';
    } else if (pagType === 'query') {
      pagLocation = 'uriQuery';
    } else {
      const err = new Error(`Pagination Type : ${pagType} not supported or insufficient data was provided`);
      return callback(null, err);
    }
    const limit = reqObj[pagLocation][paginationObject.limitVar];
    const recursiveCall = (currentOffset) => {
      try {
        const myReqObj = {
          ...reqObj
        };
        myReqObj[pagLocation][offsetVar] = currentOffset;
        this.requestHandlerInst.identifyRequest('.generic', action, myReqObj, returnF, (result, error) => {
          if (error) {
            return callback(null, error);
          }

          if (!Object.hasOwnProperty.call(result, 'response')) {
            const errorObj = this.requestHandlerInst.formatErrorObject(this.myid, meth, 'Invalid Response', ['genericAdapterRequestPaginated'], null, null, null);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            return callback(null, errorObj);
          }
          // Results hasn't been populated yet
          if (!results) {
            results = { ...result };
          } else if (paginationObject.responseDatakey) {
            setNestedProperty(results, paginationObject.responseDatakey, result);
          } else {
            results.response = results.response.concat(result.response);
          }
          if (result.response.length === limit) {
            const newOffset = incrementOffset(paginationObject.incrementBy, currentOffset, limit);
            // Rescurse
            recursiveCall(newOffset);
          } else {
            return callback(results, null);
          }
        });
      } catch (e) {
        // handle any exception
        const errorObj = this.transUtil.checkAndReturn(e, origin, 'Expanded Generic Adapter Request with Pagination Failed');
        return callback(null, errorObj);
      }
    };

    recursiveCall(reqObj[pagLocation][offsetVar]);
  }

  /**
   * Makes the requested generic call
   *
   * @function genericAdapterRequest
   * @param {string} uriPath - the path of the api call - do not include the host, port, base path or version (required)
   * @param {string} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
   * @param {object} queryData - the parameters to be put on the url (optional).
   * @param {object} requestBody - the body to add to the request (optional).
   * @param {object} addlHeaders - additional headers to be put on the call (optional).
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    return this.expandedGenericAdapterRequest(null, uriPath, restMethod, null, queryData, requestBody, addlHeaders, callback);
  }

  /**
   * Makes the requested generic call with no base path or version
   *
   * @function genericAdapterRequestNoBasePath
   * @param {string} uriPath - the path of the api call - do not include the host, port, base path or version (required)
   * @param {string} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
   * @param {object} queryData - the parameters to be put on the url (optional).
   * @param {object} requestBody - the body to add to the request (optional).
   * @param {object} addlHeaders - additional headers to be put on the call (optional).
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    return this.expandedGenericAdapterRequest({ basepath: 'NOBASE' }, uriPath, restMethod, null, queryData, requestBody, addlHeaders, callback);
  }
}

module.exports = GenericHandler;
