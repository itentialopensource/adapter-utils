{
  "$id": "action-schemas",
  "type": "object",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "properties": {
    "actions": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "description": "the action being performed",
            "examples": [
              "get_config"
            ]
          },
          "protocol": {
            "type": "string",
            "description": "the protocol to use for this action",
            "default": "REST",
            "enum": [
              "REST"
            ]
          },
          "method": {
            "type": "string",
            "description": "the method to use for this action",
            "default": "GET",
            "enum": [
              "GET", "POST", "PUT", "DELETE", "PATCH", "HEAD", "OPTIONS", "TRACE", "CONNECT"
            ]
          },
          "entitypath": {
            "type": [
              "string",
              "object"
            ],
            "description": "the path(s) to use for this action",
            "default": "",
            "examples": [
              "/api/devices{pathv1}{query}", "/api/devices{pathv1}/components{pathv2}{query}",
              "/api/devices", "/api/devices{query}"
            ]
          },
          "querykey": {
            "type": "string",
            "description": "anything that should proceed the query in the entitypath",
            "default": "?",
            "examples": [
              "?query=", "?sysparam_query="
            ]
          },
          "timeout": {
            "type": "integer",
            "description": "request timeout for this action"
          },
          "sendEmpty": {
            "type": "boolean",
            "description": "whether to send an empty body"
          },
          "sendGetBody": {
            "type": "boolean",
            "description": "whether to send a body with the get request"
          },
          "schema": {
            "type": "string",
            "description": "the name of the schema file for this action",
            "examples": [
              "schema.json"
            ]
          },
          "requestSchema": {
            "type": "string",
            "description": "the name of the request schema file for this action (will override the schema)",
            "examples": [
              "req_schema.json"
            ]
          },
          "responseSchema": {
            "type": "string",
            "description": "the name of the response schema file for this action (will override the schema)",
            "examples": [
              "resp_schema.json"
            ]
          },
          "datatype": {
            "type": "string",
            "description": "the type of data used in this action, if unsupported type JSON will be used",
            "default": "JSON"
          },
          "requestDatatype": {
            "type": "string",
            "description": "the name of the request data type for this action (will override the datatype)",
            "examples": [
              "req_schema.json"
            ]
          },
          "responseDatatype": {
            "type": "string",
            "description": "the name of the response data type for this action (will override the datatype)",
            "examples": [
              "resp_schema.json"
            ]
          },
          "sso": {
            "$ref": "#/definitions/sso"
          },
          "headers": {
            "type": "object"
          },
          "responseObjects": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "type": {
                  "type": "string",
                  "description": "the response type - based on the entitypath",
                  "default": "default",
                  "examples": [
                    "default", "withpathv1", "withpathv2", "withpathv3", "withquery"
                  ]
                },
                "key": {
                  "type": "string",
                  "description": "the field in the response where the data we want is",
                  "default": "",
                  "examples": [
                    "result"
                  ]
                },
                "mockFile": {
                  "type": "string",
                  "description": "the relative location of a data file containing a mock response",
                  "examples": [
                    "mockdatafiles/actionname.json"
                  ]
                }
              },
              "required": ["type", "key"]
            }
          }
        },
        "required": ["name", "protocol", "method", "entitypath"],
        "oneOf": [
          {
            "required": ["schema"]
          },
          {
            "required": ["requestSchema","responseSchema"]
          }      
        ]
      }
    }
  },
  "required": ["actions"],
  "definitions": {
    "sso": {
      "type": "object",
      "properties": {
        "protocol": {
          "type": "string",
          "description": "the protocol to request token from system",
          "default": "",
          "enum": [
            "http", "https", ""
          ]
        },
        "host": {
          "type": "string",
          "description": "hostname of the authentication system",
          "default": "",
          "examples": [
            "systemx.customer.com"
          ]
        },
        "port": {
          "type": "integer",
          "description": "port on which to connect to the authentication system",
          "default": 0,
          "minimum": 0,
          "maximum": 65535
        }
      }
    }
  }
}