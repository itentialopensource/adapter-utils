module.exports = {
  env: {
    mocha: true
  },
  globals: {
    log: true
  },
  rules: {
    // mocha discourages the use of lambda functions 😢
    // https://mochajs.org/#arrow-functions
    'func-names': 'off',
    'prefer-arrow-callback': 'off',
    // We want to isolate in our tests.
    'global-require': 'off',
    'max-len': 'warn'
  }
};
