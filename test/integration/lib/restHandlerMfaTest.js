const assert = require('assert');
const nock = require('nock');
const fs = require('fs');
const winston = require('winston');

let logLevel = 'none';

// Create adapter with unique Id for each testcase to avoid side effects
let adapterId = 0;
const getAdapterId = () => {
  adapterId += 1;
  return `mfa${adapterId}`;
};

const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});
// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

const entityName = 'template_entity';
const schemaGet = JSON.parse(fs.readFileSync('test/utilsinternalget.json', 'utf-8'));

const PropUtil = require('../../../lib/propertyUtil');
const Connector = require('../../../lib/connectorRest');
const TransUtil = require('../../../lib/translatorUtil');
const RestHandler = require('../../../lib/restHandler');

const adapterProperties = require('./properties/2_step_mfa.json');
const adapterPropertiesNested = require('./properties/2_step_mfa_nested.json');
const adapterProperties5Step = require('./properties/5_step_mfa.json');
const adapterProperties2stepTokenTimeout0 = require('./properties/2_step_mfa_token_timeout_0.json');

describe('[integration] Adapter Library Test', () => {
  describe('Authentication tests', () => {
    describe('# 2 step authentication', () => {
      it('2 step auth, verify user request is send with final MFA token in <xsx-authorization> header, step-1 HTTP 401, successfullResponseCode hit', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: {}
        };
        const mfaResponseStep2 = {
          headers: {},
          body: {
            token: 'FINAL-TOKEN',
            expiry_at: '2033-12-17T03:24:00'
          }
        };

        const { response } = require('../../entities/template_entity/mockdatafiles/getentitysingle.json');

        /* Authentication flow and message contents
            Client                                                                                          Server
               ----request /v3/auth/tokens (body{username, password}, headers{})------------------------------>
               <---response (body{}, headers{session: 'USER-SESSION-1'})---------------------------------------
               ----request /v3/auth/tokens (body{totp}, headers{jx-session:'USER-SESSION-1'})----------------->
               <---response (body{token: 'FINAL-TOKEN'}, headers{})--------------------------------------------
               ----request /api/now/table/change_request (body{}, headers{xsx-authorization:'FINAL-TOKEN'})--->
        */
        // Step-1 request
        const scope1 = nock('http://dot.com')
          .matchHeader('Secret-Base-64-Encoded', 'bnVsbDpudWxs')
          .post('/v3/auth/tokens', {
            username: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(401, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request
        const scope2 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(201, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Authenticated request
        const scope3 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': `Bearer ${mfaResponseStep2.body.token}`
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, error);
          assert.equal(result.icode, 'AD.200');
          const expected = {
            id: 'a9e9c33dc61122760072455df62663d2',
            number: 'CHG0000001',
            summary: 'Rollback Oracle Version',
            description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
            category: 'Software'
          };
          assert.deepEqual(result.response, expected);
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          assert.ok(scope2.isDone());
          assert.ok(scope3.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 step auth, verify user request is send with final MFA token in <xsx-authorization> header, nested token step-1 response', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step-nested');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterPropertiesNested,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterPropertiesNested,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: { data: { secret: { supertoken: 'SUPER-TOKEN-IS-HERE' } } }
        };
        const mfaResponseStep2 = {
          headers: {},
          body: {
            token: 'FINAL-TOKEN',
            expiry_at: '2033-12-17T03:24:00'
          }
        };

        const { response } = require('../../entities/template_entity/mockdatafiles/getentitysingle.json');

        // Step-1 request
        const scope1 = nock('http://dot.com')
          .matchHeader('Secret-Base-64-Encoded', 'bnVsbDpudWxs')
          .post('/v3/auth/tokens', {
            username: adapterPropertiesNested.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterPropertiesNested.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterPropertiesNested.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterPropertiesNested.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterPropertiesNested.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(401, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request
        const scope2 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterPropertiesNested.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword,
          supToken: mfaResponseStep1.body.data.secret.supertoken
        }).reply(201, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Authenticated request
        const scope3 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': `Bearer ${mfaResponseStep2.body.token}`
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, error);
          assert.equal(result.icode, 'AD.200');
          const expected = {
            id: 'a9e9c33dc61122760072455df62663d2',
            number: 'CHG0000001',
            summary: 'Rollback Oracle Version',
            description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
            category: 'Software'
          };
          assert.deepEqual(result.response, expected);
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          assert.ok(scope2.isDone());
          assert.ok(scope3.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 step auth, verify user request is send with final MFA token in <xsx-authorization> header, step-1 HTTP 401, successfullResponseCode hit, step 1 redirection HTTP 301', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: {}
        };
        const mfaResponseStep2 = {
          headers: {},
          body: {
            token: 'FINAL-TOKEN',
            expiry_at: '2033-12-17T03:24:00'
          }
        };

        const { response } = require('../../entities/template_entity/mockdatafiles/getentitysingle.json');

        // Step-1 request
        const scope1 = nock('http://dot.com')
          .matchHeader('Secret-Base-64-Encoded', 'bnVsbDpudWxs')
          .post('/v3/auth/tokens', {
            username: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(301, {}, { location: '/v3/auth/supertokens' });
        // Step-1 request to redirected location
        const scope2 = nock('http://dot.com')
          .matchHeader('Secret-Base-64-Encoded', 'bnVsbDpudWxs')
          .post('/v3/auth/supertokens', {
            username: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(401, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request
        const scope3 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(201, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Authenticated request
        const scope4 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': `Bearer ${mfaResponseStep2.body.token}`
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, error);
          assert.equal(result.icode, 'AD.200');
          const expected = {
            id: 'a9e9c33dc61122760072455df62663d2',
            number: 'CHG0000001',
            summary: 'Rollback Oracle Version',
            description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
            category: 'Software'
          };
          assert.deepEqual(result.response, expected);
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          assert.ok(scope2.isDone());
          assert.ok(scope3.isDone());
          assert.ok(scope4.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 step auth, verify user request is send with final MFA token in <xsx-authorization> header, step-1 HTTP 201, successfullResponseCode miss', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: {}
        };
        const mfaResponseStep2 = {
          headers: {},
          body: {
            token: 'FINAL-TOKEN',
            expiry_at: '2033-12-17T03:24:00'
          }
        };

        const { response } = require('../../entities/template_entity/mockdatafiles/getentitysingle.json');

        // Step-1 request
        const scope1 = nock('http://dot.com')
          .post('/v3/auth/tokens', {
            username: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(201, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request
        const scope2 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(201, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Authenticated request
        const scope3 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': `Bearer ${mfaResponseStep2.body.token}`
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, error);
          assert.equal(result.icode, 'AD.200');
          const expected = {
            id: 'a9e9c33dc61122760072455df62663d2',
            number: 'CHG0000001',
            summary: 'Rollback Oracle Version',
            description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
            category: 'Software'
          };
          assert.deepEqual(result.response, expected);
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          assert.ok(scope2.isDone());
          assert.ok(scope3.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 step auth, 1 step configuration mismatch, wrong external_name in response schema', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { 'unexpected-session': 'USER-SESSION-1' },
          body: {}
        };

        // Step-1 request
        const scope1 = nock('http://dot.com')
          .matchHeader('Secret-Base-64-Encoded', 'bnVsbDpudWxs')
          .post('/v3/auth/tokens', {
            username: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(401, mfaResponseStep1.body, mfaResponseStep1.headers);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, result);
          assert.equal(error.message, 'Response schema for step-1 misconfiguration');
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 step auth, first step failure, user request fails due to lack of authorization', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };

        const scope1 = nock('http://dot.com')
          .post('/v3/auth/tokens')
          .reply(500, {}, {});

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(error.icode, 'AD.400');
          assert.equal(error.IAPerror.displayString, 'Unable to authenticate with Token, response 500');
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 step auth, last step failure, user request fails due to lack of authorization', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: {
            token: 'SECRET-STEP-1'
          }
        };
        const scope1 = nock('http://dot.com')
          .post('/v3/auth/tokens')
          .reply(200, mfaResponseStep1.body, mfaResponseStep1.headers)
          .post('/v3/auth/tokens')
          .reply(500, {}, {});

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(error.icode, 'AD.400');
          assert.equal(error.IAPerror.displayString, 'Unable to authenticate with Token, response 500');
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 step auth, 1st step failure, auth host not reachable', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };

        nock.disableNetConnect();

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(error.icode, 'AD.400');
          assert.equal(error.IAPerror.displayString, 'Unable to authenticate with Token, response -1');
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });

      it('2 subsequent requests, verify 1st obtains the token from server, 2nd obtains same token from cache, token_timeout set', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: {}
        };
        const mfaResponseStep2 = {
          headers: {},
          body: {
            token: 'FINAL-TOKEN',
            expiry_at: '2033-12-17T03:24:00'
          }
        };

        const { response } = require('../../entities/template_entity/mockdatafiles/getentitysingle.json');

        // Step-1 request
        const scope1 = nock('http://dot.com')
          .post('/v3/auth/tokens', {
            username: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(200, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request
        const scope2 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(200, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Authenticated request
        const scope3 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': `Bearer ${mfaResponseStep2.body.token}`
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);
        // Step-1 request // should not execute due to token caching
        const scope4 = nock('http://dot.com')
          .post('/v3/auth/tokens', {
            username: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.password,
            grant_type: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.grant_type,
            client_secret: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_secret,
            client_id: adapterProperties.authentication.multiStepAuthCalls[0].requestFields.client_id
          }).reply(200, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request // should not execute due to token caching
        const scope5 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(200, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Subsequent authenticated request with cached token, no MFA steps triggered
        const scope6 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': `Bearer ${mfaResponseStep2.body.token}`
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, error);
          assert.equal(result.icode, 'AD.200');
          const expected = {
            id: 'a9e9c33dc61122760072455df62663d2',
            number: 'CHG0000001',
            summary: 'Rollback Oracle Version',
            description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
            category: 'Software'
          };
          assert.deepEqual(result.response, expected);
          // 2nd request shall reuse the token from cache
          handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result2, error2) => {
            assert.equal(null, error2);
            assert.equal(result2.icode, 'AD.200');
            const expected2 = {
              id: 'a9e9c33dc61122760072455df62663d2',
              number: 'CHG0000001',
              summary: 'Rollback Oracle Version',
              description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
              category: 'Software'
            };
            assert.deepEqual(result2.response, expected2);
            // verify token requests were sent only for initial requests, 2nd request shall not trigger MFA
            assert.ok(scope1.isDone());
            assert.ok(scope2.isDone());
            assert.ok(scope3.isDone());
            assert.ok(!scope4.isDone());
            assert.ok(!scope5.isDone());
            assert.ok(scope6.isDone());
            nock.cleanAll();
            done();
          }).catch((err) => done(err));
        }).catch((error) => done(error));
      });

      it('2 subsequent requests, verify 1st obtains the token from server, 2nd obtains same token from cache, token_timeout not set', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/2-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties2stepTokenTimeout0,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties2stepTokenTimeout0,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: {}
        };
        const mfaResponseStep2 = {
          headers: {},
          body: {
            token: 'FINAL-TOKEN',
            expiry_at: '2033-12-17T03:24:00'
          }
        };

        const { response } = require('../../entities/template_entity/mockdatafiles/getentitysingle.json');

        // Step-1 request
        const scope1 = nock('http://dot.com')
          .post('/v3/auth/tokens', {
            username: adapterProperties2stepTokenTimeout0.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties2stepTokenTimeout0.authentication.multiStepAuthCalls[0].requestFields.password
          }).reply(200, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request
        const scope2 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties2stepTokenTimeout0.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(200, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Authenticated request
        const scope3 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': mfaResponseStep2.body.token
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);
        // Step-1 request // should not execute due to token caching
        const scope4 = nock('http://dot.com')
          .post('/v3/auth/tokens', {
            username: adapterProperties2stepTokenTimeout0.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties2stepTokenTimeout0.authentication.multiStepAuthCalls[0].requestFields.password
          }).reply(200, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request // should not execute due to token caching
        const scope5 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties2stepTokenTimeout0.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(200, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Subsequent authenticated request with cached token, no MFA steps triggered
        const scope6 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': mfaResponseStep2.body.token
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, error);
          assert.equal(result.icode, 'AD.200');
          const expected = {
            id: 'a9e9c33dc61122760072455df62663d2',
            number: 'CHG0000001',
            summary: 'Rollback Oracle Version',
            description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
            category: 'Software'
          };
          assert.deepEqual(result.response, expected);
          // 2nd request shall reuse the token from cache
          handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result2, error2) => {
            assert.equal(null, error2);
            assert.equal(result2.icode, 'AD.200');
            const expected2 = {
              id: 'a9e9c33dc61122760072455df62663d2',
              number: 'CHG0000001',
              summary: 'Rollback Oracle Version',
              description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
              category: 'Software'
            };
            assert.deepEqual(result2.response, expected2);
            // verify token requests were sent only for initial requests, 2nd request shall not trigger MFA
            assert.ok(scope1.isDone());
            assert.ok(scope2.isDone());
            assert.ok(scope3.isDone());
            assert.ok(!scope4.isDone());
            assert.ok(!scope5.isDone());
            assert.ok(scope6.isDone());
            nock.cleanAll();
            done();
          }).catch((err) => done(err));
        }).catch((error) => done(error));
      });
    });

    describe('# 5 step authentication', () => {
      it('5 step auth, verify user request is send with final MFA token in <xsx-authorization> header', (done) => {
        const id = getAdapterId();
        const propInst = new PropUtil(id, 'test/mfa/5-step');
        const transInst = new TransUtil(id, propInst);
        const connector = new Connector(
          id,
          adapterProperties5Step,
          transInst,
          propInst
        );
        const handler = new RestHandler(
          id,
          adapterProperties5Step,
          connector,
          transInst
        );
        const dataObj = {
          summary: 'fake summary'
        };
        const mfaResponseStep1 = {
          headers: { session: 'USER-SESSION-1' },
          body: {}
        };
        const mfaResponseStep2 = {
          headers: {},
          body: {
            token: 'subtoken-123'
          }
        };
        const mfaResponseStep3 = {
          headers: { 'secondary-session': 'USER-SESSION-1' },
          body: {}
        };
        const mfaResponseStep4 = {
          headers: {},
          body: {
            token: 'secondary-token-qwerty'
          }
        };
        const mfaResponseStep5 = {
          headers: {},
          body: {
            token: 'FINAL-TOKEN',
            expiry_at: '2033-12-17T03:24:00'
          }
        };

        const { response } = require('../../entities/template_entity/mockdatafiles/getentitysingle.json');

        // Step-1 request
        const scope1 = nock('http://dot.com')
          .post('/v3/auth/tokens', {
            username: adapterProperties5Step.authentication.multiStepAuthCalls[0].requestFields.username,
            password: adapterProperties5Step.authentication.multiStepAuthCalls[0].requestFields.password
          }).reply(200, mfaResponseStep1.body, mfaResponseStep1.headers);
        // Step-2 request
        const scope2 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          totp: adapterProperties5Step.authentication.multiStepAuthCalls[1].requestFields.timedOneTimePassword
        }).reply(200, mfaResponseStep2.body, mfaResponseStep2.headers);
        // Step-3 request
        const scope3 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session
          }
        }).post('/v3/auth/tokens', {
          token: mfaResponseStep2.body.token,
          leftIndexFingerPrint: adapterProperties5Step.authentication.multiStepAuthCalls[2].requestFields.leftIndexFingerPrint
        }).reply(200, mfaResponseStep3.body, mfaResponseStep3.headers);
        // Step-4 request
        const scope4 = nock('http://dot.com', {
          reqheaders: {
            'secondary-session': mfaResponseStep3.headers['secondary-session']
          }
        }).post('/v3/auth/tokens', {
          subtoken: mfaResponseStep2.body.token,
          leftMidleFingerPrint: adapterProperties5Step.authentication.multiStepAuthCalls[3].requestFields.leftMidleFingerPrint
        }).reply(200, mfaResponseStep4.body, mfaResponseStep4.headers);
        // Step-5 request
        const scope5 = nock('http://dot.com', {
          reqheaders: {
            'jx-session': mfaResponseStep1.headers.session,
            'secondary-token': mfaResponseStep4.body.token
          }
        }).post('/v3/auth/tokens', {
          leftRingFingerPrint: adapterProperties5Step.authentication.multiStepAuthCalls[4].requestFields.leftRingFingerPrint
        }).reply(200, mfaResponseStep5.body, mfaResponseStep5.headers);
        // Authenticated request
        const scope6 = nock('http://dot.com', {
          reqheaders: {
            'xsx-authorization': mfaResponseStep5.body.token
          }
        }).get('/api/now/table/change_request')
          .reply(200, response);

        handler.genericRestRequest(entityName, 'getEntities', schemaGet, dataObj, true, (result, error) => {
          assert.equal(null, error);
          assert.equal(result.icode, 'AD.200');
          const expected = {
            id: 'a9e9c33dc61122760072455df62663d2',
            number: 'CHG0000001',
            summary: 'Rollback Oracle Version',
            description: 'Performance of the Siebel SFA software has been severely\\n            degraded since the upgrade performed this weekend.\\n\\n            We moved to an unsupported Oracle DB version. Need to rollback the\\n            Oracle Instance to a supported version.\\n        ',
            category: 'Software'
          };
          assert.deepEqual(result.response, expected);
          // verify all nocked requests were sent
          assert.ok(scope1.isDone());
          assert.ok(scope2.isDone());
          assert.ok(scope3.isDone());
          assert.ok(scope4.isDone());
          assert.ok(scope5.isDone());
          assert.ok(scope6.isDone());
          nock.cleanAll();
          done();
        }).catch((error) => done(error));
      });
    });
  });
});
