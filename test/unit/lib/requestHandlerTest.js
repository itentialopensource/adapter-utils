// Set globals
/* eslint no-loop-func:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const winston = require('winston');

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to
// set them here always check these in with bogus data!!!
const host = 'servicenow.dummy.instance';
const username = 'user';
const password = '';

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
const pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'test',
      properties: {
        host,
        port: 443,
        version: 'v1',
        cache_location: 'local',
        authentication: {
          auth_method: 'basic user_password',
          username,
          password,
          token: 'token',
          token_user_field: 'username',
          token_password_field: 'password',
          token_result_field: 'token',
          token_URI_path: '/authenticate',
          invalid_token_error: 401,
          auth_field: 'header.auth',
          auth_field_format: '{username}:{password}'
        },
        throttle: {
          throttle_enabled: false,
          number_pronghorns: 1,
          sync_async: 'sync',
          max_in_queue: 1000,
          concurrent_max: 1,
          expire_timeout: 0,
          avg_runtime: 200
        },
        request: {
          number_retries: 3,
          limit_retry_error: 0,
          failover_codes: [401],
          attempt_timeout: attemptTimeout,
          healthcheck_on_timeout: false,
          archiving: false
        },
        proxy: {
          enabled: false,
          host: '',
          port: 1
        },
        ssl: {
          enabled: false,
          accept_invalid_cert: false,
          ca_file: '',
          ciphers: ''
        },
        healthcheck: {
          type: 'startup',
          protocol: 'REST',
          frequency: 60000,
          URI_Path: '/api/now/table/sys_user_group'
        },
        cache: {
          enabled: true,
          entities: [
            { // sample entity 1
              entityType: 'sample',
              frequency: 10,
              flushOnFail: true,
              populate: [
                {
                  path: '/get/sample/id1/status',
                  method: 'GET',
                  query: {},
                  body: {},
                  headers: {},
                  handleFailure: 'ignore',
                  requestFields: {
                    id: 'name'
                  },
                  responseDatakey: '',
                  responseFull: false,
                  responseFields: {
                    id: 'id1', // what would these values be for?
                    name: 'name1'
                  }
                }
              ]
            }
            // insert more sample entities here
          ]
        },
        protocol: 'https',
        stub
      },
      type: 'test'
    }]
  }
};
let finishedTests = 0;
let badActions = false;
let goodActions = true;

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted
// in so without this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the request handler as that is the entry into the Adapter Libraries
const ReqHandler = require('../../../lib/requestHandler');

// make sure the good action files are initially in the entities
fs.copyFileSync('test/entities/template_badaction1/actiong.json', 'test/entities/template_badaction1/action.json');
fs.copyFileSync('test/entities/template_badaction2/actiong.json', 'test/entities/template_badaction2/action.json');
fs.copyFileSync('test/entities/template_badaction3/actiong.json', 'test/entities/template_badaction3/action.json');

// begin the testing - these should be pretty well defined between the describe
// and the it!
describe('[unit] Adapter Library Test', () => {
  describe('Request Handler Class Tests', () => {
    let a = null;
    try {
      a = new ReqHandler(
        pronghornProps.adapterProps.adapters[0].id,
        pronghornProps.adapterProps.adapters[0].properties,
        'test'
      );
    } catch (e) {
      log.error(e);
    }

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        fs.exists('package.json', (val) => {
          assert.equal(true, val);
          done();
        });
      });
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        fs.exists('schemas/propertiesSchema.json', (val) => {
          assert.equal(true, val);
          done();
        });
      });
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        fs.exists('README.md', (val) => {
          assert.equal(true, val);
          done();
        });
      });
    });

    describe('#getAdapterInventory', () => {
      it('should get adapter inventory', (done) => {
        const p = new Promise((resolve) => {
          a.getAdapterInventory((data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    const prop = 'testing';
    describe('#encryptProperty', () => {
      it('should get null if no property', (done) => {
        const p = new Promise((resolve) => {
          a.encryptProperty(null, null, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.equal(null, data.response);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get property back if no technique', (done) => {
        const p = new Promise((resolve) => {
          a.encryptProperty(prop, null, (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);
            assert.equal(prop, data.response);
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get base64 encoded property', (done) => {
        const p = new Promise((resolve) => {
          a.encryptProperty(prop, 'base64', (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);
            assert.equal(0, data.response.indexOf('{code}'));
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        const p = new Promise((resolve) => {
          a.encryptProperty(prop, 'encrypt', (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.notEqual(undefined, data.response);
            assert.notEqual(null, data.response);
            assert.equal(0, data.response.indexOf('{crypt}'));
            finishedTests += 1;
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#setFailover', () => {
      it('should failover', (done) => {
        const p = new Promise((resolve) => {
          const errorObj = { code: 501 };
          const data = a.setFailover(errorObj);
          resolve(data);
          assert.equal('AD.500', data);
          finishedTests += 1;
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should not failover', (done) => {
        const p = new Promise((resolve) => {
          const errorObj = { code: 401 };
          const data = a.setFailover(errorObj);
          resolve(data);
          assert.equal('AD.300', data);
          finishedTests += 1;
          done();
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('populateEntityCache', () => {
      it('Populate errors due to no associated properties', (done) => {
        const p = new Promise(() => {
          a.populateEntityCache(['playbooks'], (data, error) => {
            // resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            done();
          });
        });
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('retrieveEntitiesCache', () => {
      a.cacheHandler.cache.push({
        lockKey: 'testkey',
        entityType: 'template_entity',
        list: [{
          id: 'id',
          name: 'name'
        }],
        intervalId: 'interval'
      }); // note: missing associated props
      it('should find entity', (done) => {
        const p = new Promise((resolve) => {
          a.retrieveEntitiesCache('template_entity', {}, (data, error) => {
            resolve(data);
            assert.notEqual(null, data);
            assert.equal(undefined, error);
            done();
          });
        });
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('checkEntityCached', () => {
      it('should not find entity type', (done) => {
        const p = new Promise((resolve) => {
          a.checkEntityCached('not_template_entity', 'name', (data, error) => {
            resolve(data);
            assert.equal(false, data);
            assert.equal(undefined, error);
            done();
          });
        });
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should not find entity id', (done) => {
        const p = new Promise((resolve) => {
          a.checkEntityCached('template_entity', 'badname', (data, error) => {
            resolve(data);
            assert.equal(1, data.length);
            assert.equal('notfound', data[0]);
            assert.equal(undefined, error);
            done();
          });
        });
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should find entity', (done) => {
        const p = new Promise((resolve) => {
          a.checkEntityCached('template_entity', 'name', (data, error) => {
            resolve(data);
            assert.equal(1, data.length);
            assert.equal('found', data[0]);
            assert.equal(undefined, error);
            done();
          });
        });
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should find first entity and not find second entity', (done) => {
        const p = new Promise((resolve) => {
          a.checkEntityCached('template_entity', ['name', 'badname'], (data, error) => {
            resolve(data);
            assert.equal(2, data.length);
            assert.equal('found', data[0]);
            assert.equal('notfound', data[1]);
            assert.equal(undefined, error);
            done();
          });
        });
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#verifyCapability', () => {
      it('should not verify the capability', (done) => {
        const p = new Promise((resolve) => {
          a.verifyCapability('not_template_entity', null, 'randomId', (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.notEqual(undefined, data);
            assert.notEqual(null, data);
            assert.equal('notfound', data[0]);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
      it('should verify the capability', (done) => {
        const p = new Promise((resolve) => {
          a.verifyCapability('template_entity', null, 'name', (data, error) => {
            resolve(data);
            assert.equal(undefined, error);
            assert.equal('found', data[0]);
            done();
          });
        });
        // log just done to get rid of const lint issue!
        log.debug(p);
      }).timeout(attemptTimeout);
    });

    describe('#hasEntities', () => {
      it('should error due to no entitytype', (done) => {
        const p = new Promise((resolve) => {
          a.hasEntities(null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.equal('test does not support entity null', error);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
      it('should error due to invalid entitytype', (done) => {
        const p = new Promise((resolve) => {
          a.hasEntities('fake', null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.equal('test does not support entity fake', error);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#getDevice', () => {
      it('should error due to no broker call', (done) => {
        const p = new Promise((resolve) => {
          a.getDevice(null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.999', error.icode);
            assert.equal('test-brokerHandler-getDevice', error.IAPerror.origin);
            assert.equal('Missing Properties devicebroker.getDevice.path', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesFiltered', () => {
      it('should error due to no broker call', (done) => {
        const p = new Promise((resolve) => {
          a.getDevicesFiltered(null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.999', error.icode);
            assert.equal('test-brokerHandler-getDevicesFiltered', error.IAPerror.origin);
            assert.equal('Missing Properties devicebroker.getDevicesFiltered.path', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#isAlive', () => {
      it('should error due to no broker call', (done) => {
        const p = new Promise((resolve) => {
          a.isAlive(null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.999', error.icode);
            assert.equal('test-brokerHandler-isAlive', error.IAPerror.origin);
            assert.equal('Missing Properties devicebroker.isAlive.path', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#getConfig', () => {
      it('should error due to no broker call', (done) => {
        const p = new Promise((resolve) => {
          a.getConfig(null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.999', error.icode);
            assert.equal('test-brokerHandler-getConfig', error.IAPerror.origin);
            assert.equal('Missing Properties devicebroker.getConfig.path', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount', () => {
      it('should error due to no broker call', (done) => {
        const p = new Promise((resolve) => {
          a.iapGetDeviceCount((data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.999', error.icode);
            assert.equal('test-brokerHandler-iapGetDeviceCount', error.IAPerror.origin);
            assert.equal('Missing Properties devicebroker.getCount.path', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#expandedGenericAdapterRequest', () => {
      it('should error due to missing uriPath', (done) => {
        const p = new Promise((resolve) => {
          a.expandedGenericAdapterRequest(null, null, null, null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-genericHandler-expandedGenericAdapterRequest', error.IAPerror.origin);
            assert.equal('uriPath is required', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
      it('should error due to missing restMethod', (done) => {
        const p = new Promise((resolve) => {
          a.expandedGenericAdapterRequest(null, 'fake/url', null, null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-genericHandler-expandedGenericAdapterRequest', error.IAPerror.origin);
            assert.equal('restMethod is required', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#genericAdapterRequest', () => {
      it('should error due to missing uriPath', (done) => {
        const p = new Promise((resolve) => {
          a.genericAdapterRequest(null, null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-genericHandler-expandedGenericAdapterRequest', error.IAPerror.origin);
            assert.equal('uriPath is required', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
      it('should error due to missing restMethod', (done) => {
        const p = new Promise((resolve) => {
          a.genericAdapterRequest('fake/url', null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-genericHandler-expandedGenericAdapterRequest', error.IAPerror.origin);
            assert.equal('restMethod is required', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should error due to missing uriPath', (done) => {
        const p = new Promise((resolve) => {
          a.genericAdapterRequestNoBasePath(null, null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-genericHandler-expandedGenericAdapterRequest', error.IAPerror.origin);
            assert.equal('uriPath is required', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
      it('should error due to missing restMethod', (done) => {
        const p = new Promise((resolve) => {
          a.genericAdapterRequestNoBasePath('fake/url', null, null, null, null, (data, error) => {
            resolve(data);
            assert.equal(null, data);
            assert.notEqual(undefined, error);
            assert.notEqual(null, error);
            assert.notEqual(undefined, error.IAPerror);
            assert.notEqual(null, error.IAPerror);
            assert.notEqual(undefined, error.IAPerror.displayString);
            assert.notEqual(null, error.IAPerror.displayString);
            assert.equal('AD.300', error.icode);
            assert.equal('test-genericHandler-expandedGenericAdapterRequest', error.IAPerror.origin);
            assert.equal('restMethod is required', error.IAPerror.displayString);
            done();
          });
          // log just done to get rid of const lint issue!
          log.debug(p);
        });
      }).timeout(attemptTimeout);
    });

    const entityName = 'template_entity';
    const actionName = 'getEntities';
    const badEntityAction1 = 'template_badaction1';
    const badEntityAction2 = 'template_badaction2';
    const badEntityAction3 = 'template_badaction3';
    const pretestcnt = 7;

    // need to wait so that we can insert the bas action files
    const intervalObject = setInterval(() => {
      if (finishedTests >= pretestcnt && !badActions) {
        // make sure the good action files are initially in the entities
        fs.copyFileSync('test/entities/template_badaction1/actionb.json', 'test/entities/template_badaction1/action.json');
        fs.copyFileSync('test/entities/template_badaction2/actionb.json', 'test/entities/template_badaction2/action.json');
        if (fs.existsSync('test/entities/template_badaction3/action.json')) {
          fs.unlinkSync('test/entities/template_badaction3/action.json');
        }
        badActions = true;
        goodActions = false;
        clearInterval(intervalObject);
      }
    }, 1000);

    describe('#identifyRequest - errors', () => {
      it('error on identify request - no entity', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(null, null, null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.300', error.icode);
                assert.equal('test-requestHandler-identifyRequest', error.IAPerror.origin);
                assert.equal('entity is required', error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - no action', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(entityName, null, null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.300', error.icode);
                assert.equal('test-requestHandler-identifyRequest', error.IAPerror.origin);
                assert.equal('action is required', error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - bad entity', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest('garbage', actionName, null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.301', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/garbage/action.json';
                assert.equal(`Can not open file ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - missing action', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(entityName, 'garbage', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.302', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_entity/action.json';
                assert.equal(`Invalid action file: missing action in ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - missing action file', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction3, 'missingActionFile', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.301', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_badaction3/action.json';
                assert.equal(`Can not open file ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - missing actions array', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction2, 'missingActionsArray', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.302', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_badaction2/action.json';
                assert.equal(`Invalid action file: missing array of actions in ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - action missing protocol', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction1, 'missingProtocol', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.302', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_badaction1/action.json';
                assert.equal(`Invalid action file: missing protocol in ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - action bad protocol', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction1, 'badProtocol', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.303', error.icode);
                assert.equal('test-requestHandler-identifyRequest', error.IAPerror.origin);
                assert.equal('Protocol ANOTHER not currently supported', error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - action missing method', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction1, 'missingMethod', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.302', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_badaction1/action.json';
                assert.equal(`Invalid action file: missing method in ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - action missing path', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction1, 'missingPath', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.302', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_badaction1/action.json';
                assert.equal(`Invalid action file: missing entity path in ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - action missing schema', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction1, 'missingSchema', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.302', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_badaction1/action.json';
                assert.equal(`Invalid action file: missing schema in ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      it('error on identify request - action missing schema file', (done) => {
        const interval = setInterval(() => {
          if (finishedTests >= pretestcnt && badActions) {
            clearInterval(interval);
            const p = new Promise((resolve) => {
              a.identifyRequest(badEntityAction1, 'missingSchemaFile', null, null, (data, error) => {
                resolve(data);
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error.IAPerror);
                assert.notEqual(null, error.IAPerror);
                assert.notEqual(undefined, error.IAPerror.displayString);
                assert.notEqual(null, error.IAPerror.displayString);
                assert.equal('AD.301', error.icode);
                assert.equal('test-propertyUtil-getEntitySchemaFromFS', error.IAPerror.origin);
                const path = 'test/entities/template_badaction1/anotherschema.json';
                assert.equal(`Can not open file ${path}`, error.IAPerror.displayString);
                finishedTests += 1;
                done();
              });
              // log just done to get rid of const lint issue!
              log.debug(p);
            });
          }
        }, 1000);
      }).timeout(attemptTimeout);
      if (stub) {
        it('identify request - no mock data error', (done) => {
          const interval = setInterval(() => {
            if (finishedTests >= pretestcnt && badActions) {
              clearInterval(interval);
              const p = new Promise((resolve) => {
                const dataObj = {
                  uriQuery: {
                    number: 'CHG0000001'
                  }
                };
                a.identifyRequest(entityName, 'getEntitiesNoMock', dataObj, true, (data, error) => {
                  resolve(data);
                  assert.equal(null, data);
                  assert.notEqual(undefined, error);
                  assert.notEqual(null, error);
                  assert.notEqual(undefined, error.IAPerror);
                  assert.notEqual(null, error.IAPerror);
                  assert.notEqual(undefined, error.IAPerror.displayString);
                  assert.notEqual(null, error.IAPerror.displayString);
                  assert.equal('AD.500', error.icode);
                  assert.equal('test-connectorRest-handleEndResponse', error.IAPerror.origin);
                  assert.equal('Error 400 received on request', error.IAPerror.displayString);
                  finishedTests += 1;
                  done();
                });
                // log just done to get rid of const lint issue!
                log.debug(p);
              });
            }
          }, 1000);
        }).timeout(attemptTimeout);
      }
    });

    const posttestcnt = pretestcnt + 13;
    const intervalObject2 = setInterval(() => {
      if (finishedTests >= posttestcnt && !goodActions) {
        // make sure the good action files are initially in the entities
        fs.copyFileSync('test/entities/template_badaction1/actiong.json', 'test/entities/template_badaction1/action.json');
        fs.copyFileSync('test/entities/template_badaction2/actiong.json', 'test/entities/template_badaction2/action.json');
        fs.copyFileSync('test/entities/template_badaction3/actiong.json', 'test/entities/template_badaction3/action.json');
        goodActions = true;
        badActions = false;
        clearInterval(intervalObject2);
      }
    }, 1000);
  });
});
